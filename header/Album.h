#ifndef ALBUM_H
#define ALBUM_H

// Librairies
#include <iostream>
#include <chrono>

/**
 * \file Album.h
 * \brief Represent an Album
 * \version 0.0.1
 * \author LECOMTE Victor && DAVID Corentin 
 */
class Album {

private :
  unsigned int id; ///< Album identifier
  unsigned int date; ///< Album date as date
  std::string name; ///< Album name as string

public :
// Default constructor
  /**
   * \brief Default constructor
   */
  Album();

// Constructor with parameters
  /**
   * \brief Constructors with differents parameters
   */
  Album(unsigned int, unsigned int ,std::string);
// Destructive
  /**
   * \brief Destuctor
   */
  ~Album();

// Accessors (getters)
  /**
   * \brief Allows the recovery of the identifier
   */
  unsigned int getId();

  /**
   * \brief Allows the recovery of thre date 
   */
  unsigned int getDate();
  /**
   * \brief Allows the recovery of the name
   */
  std::string getName();

// Mutators (setters)
  /**
   * \brief Allows the modification of the date
   */
  void setDate(unsigned int);
  /**
   * \brief Allows the modification of the name
   */
  void setName(std::string);
};
#endif //ALBUM_H  
