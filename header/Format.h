#ifndef FORMAT_H
#define FORMAT_H

// Librairies
#include <string>

/**
 * \file Format.h
 * \brief Represent a Format
 * \version 0.0.1
 * \author LECOMTE Victor && DAVID Corentin
 */
class Format {

private :
  //Attibutes
  unsigned int id; ///< Format identifier
  std::string titule; ///< Formart titule as string

public :
  // Default constructor
  /**
   * \brief Default constructor 
   */
  Format();

// Constructor with parameters
  /**
   * \brief Constuctor with parmeters
   */
  Format(unsigned int,std::string);

  // Destructive
  /**
   * \brief Destructor
   */
  ~Format();

  // Accessors (getters)
  /**
   * \brief Allows the recovery of the id
   */
  unsigned int getId();
  /**
   * \brief Allows the recovery of the titule
   */
  std::string getTitule();

  // Mutators (setters)
  /**
   * \brief Allows the modification of the titule
   */
  void setTitule(std::string);
};
#endif //FORMAT_H
