#ifndef POLYPHONY_H
#define POLYPHONY_H

// Librairies
#include <string>

/**
 * \file Polyphony.h
 * \brief Reprensent a Polyphony
 * \version 0.0.1
 * \author LECOMTE Victor && DAVID Corentin
 */
class Polyphony {
private:
  // Attributes
  unsigned int id; ///< Polyphony identifier
  std::string type; ///< Polyphony type as string

public:
// Default constructor
  /**
   * \brief Default constructor
   */
  Polyphony();
// Constructor with parameters
  /**
   * \brief Constructor with parameters
   */
  Polyphony(unsigned int, std::string);

  // Destructive
  /**
   * \brief Destructor
   */
  ~Polyphony();

  // Accessors (getters)
  /**
   * \brief Allows the recovery of the id
   */
  unsigned int getId();
  /**
   * \brief Allows the recovery of type
   */
  std::string getType();

  // Mutators (setters)
  /**
   * \brief Allows the modification of type
   */
  void setType(std::string);
};
#endif // POLYPHONY_H
