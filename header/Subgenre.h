#ifndef SUBGENRE_H
#define SUBGENRE_H

// Librairies
#include <string>

/**
 *\file Subgenre.h
 *\brief Represent a Subgenre
 *\version 0.0.1
 *\author LECOMTE Victor && DAVID Corentin
 */
class Subgenre {
private:
  // Attributes
  unsigned int id; ///< Polyphony identifier
  short int sub_type; ///< Polyphonu sub_type as string

public:
  // Default constructors
  /**
   * \brief Default constructor 
   */
  Subgenre();
  // Constructor with parameters
  /**
   * \brief Constructor with parameters
   */
  Subgenre(unsigned int, short int);

  // Destructive
  /**
   * \brief Destructor
   */
  ~Subgenre();

  // Accessors (getters)
  /**
   * \brief Allows the recovery of the id 
   */
  unsigned int GetId();
  /**
   * \brief Allows the recovery of the sub_type 
   */
  short int getSub_Type();

  //  Mutators (setters)
  /**
   * \brief Allows the modification of the sub_type
   */
  void setSubtype(short int);
};
#endif //SUBGENRE_H
