#include <gflags/gflags.h>
#include <gflags/gflags_declare.h>

DECLARE_uint64(duration);
DECLARE_string(title);
DECLARE_string(artist);
DECLARE_string(album);
DECLARE_bool(format);
DECLARE_string(genre);
DECLARE_uint64(polyphony);
DECLARE_string(subgenre);
