#include <iostream>
#include <chrono>
#include <time.h>
#include "../header/Album.h"

// Default constructor
Album::Album() :
  id(1),
  date(2019),
  name("L'evrest")
{
  std::cout << "Le constructeur par défaut se compose de la façon suivante" << std::endl;
  std::cout << "L'id de l'Album est : " << id << std::endl;
  std::cout << "La date de l'Album est : " << date << std::endl;
  std::cout << "Le nom de l'Album est : " << name << std::endl;
}

// Constructor with parameters
Album::Album(unsigned int _id, unsigned int _date, std::string _name) :
  id(_id),
  date(_date),
  name(_name)
{
  std::cout << "Constructor with parameters" << std::endl;
  std::cout << "L'id de l'Album est : " << id << std::endl;
  std::cout << "La date de l'Album est : " << date << std::endl;
  std::cout << "Le nom de l'Album est : " << name << std::endl;
}

// Destructive
Album::~Album() {}

// Accessors (getters)
unsigned int Album::getId()
{
  return id;
}

std::string Album::getName()
{
  return name;
}
unsigned int Album::getDate()
{
  return date;
}

// Mutatore (setters)
void Album::setName(std::string _name)
{
  name = _name;
}

void Album::setDate(unsigned int _date)
{
  date = _date;
}
