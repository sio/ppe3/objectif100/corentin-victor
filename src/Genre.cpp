// Librairies
#include "../header/Genre.h"
#include <string>
#include <iostream>

//Default constructor
Genre::Genre() :
  id(),
  type("Hip-Hop")

{
  std::cout << " " << std::endl;
  std::cout << "Default constructors of the class Genre" << std::endl;
  std::cout << "L'id du genre est : " << id << std::endl;
  std::cout << "Le type du genre est : " << type << std::endl;
}

//Constructor with parameters
Genre::Genre(unsigned int  _id,
             std::string _type
            ) :
  id(_id),
  type(_type)
{
  std::cout << " " << std::endl;
  std::cout << "Constructor with parameters" << std::endl;
  std::cout << "L'id du genre est : " << id << std::endl;
  std::cout << "Le type du genre est : " << type << std::endl;
}

//Destructive
Genre::~Genre() {}

// Accessors (getters)
unsigned int Genre::getId()
{
  return id;
}

std::string Genre::getType()
{
  return type;
}

// Mutators (setters)
void Genre::setType(std::string _type)
{
  type = _type;
}
