#include <iostream>
#include <gflags/gflags.h> // #include <google/gflags.h>                                                                                                                                                                                                                                                                                                                                                                               
using namespace std;

DEFINE_uint64(duration, 60, "Duration of the playlist");
DEFINE_string(title, "Great plalist", "Title of the playlist");
DEFINE_string(artist, "Eminem", "Artist of the playlist");
DEFINE_string(album, "album", "Album of the playlist");
DEFINE_bool(format, true, "Format M3U or XSPF");
DEFINE_string(genre, "rock", "Genre of the playlist");
DEFINE_uint64(polyphony, 5, "Polyphony of the playlist");
DEFINE_string(subgenre, " ", "Subgenre of the playlist");

int main(int argc, char **agrv)
{
  ::google::ParseCommandLineFlags(&argc, &agrv, true);
  // std::cout << "Default duration : " << FLAGS_duration << " seconds" << std::endl;                                                                                                                                                                                                                                                                                                                                                  

  if(FLAGS_duration == 60)
  {
    std::cout << "Default duration : " << FLAGS_duration << " seconds" << std::endl;
    cout << "Default duration" << endl;
  }
  else if(FLAGS_duration > 60)
  {
    std::cout << "Duration : " << FLAGS_duration << " seconds" << std::endl;
    std::cout << FLAGS_duration <<  " >  " <<  "60" << std::endl;
  }
  else if(FLAGS_duration < 60)
  {
    std::cout << "Duration : " << FLAGS_duration << " seconds" << std::endl;
    std::cout << FLAGS_duration <<  " <  " <<  "60" << std::endl;
  }

  if(FLAGS_duration == 0)
  {
    std::cout << "It is not possible to have a playlist at 0 seconds" << FLAGS_duration << std::endl;
  }

  std::cout << "Le titre de la playlist est : " << FLAGS_title << std::endl;
  std::cout << "Le nom de l'artiste est : " << FLAGS_artist << std::endl;
  std::cout << "Le nom de l'album est : " << FLAGS_album << std::endl;
  std::cout << "Le format est : " << FLAGS_format << std::endl;
  std::cout << "Le nom du genre est : " << FLAGS_genre << std::endl;
  std::cout << "La polyphony ets de " << FLAGS_polyphony << " cannaux" <<std::endl;
  std::cout << "Le nom du sous genre est : " << FLAGS_subgenre << std::endl;

  return 0;
}


