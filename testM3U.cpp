#include <iostream>
#include <string>
#include <fstream>
#include <list>
 
using namespace std;
 
int main()
{
        ofstream fichier("playlis.m3u", ios::out | ios::trunc);// Declaration of the flow and opening of the file
        
        if(fichier)  // If the opening is succesfu
        {
          
          std::cout << "Here is the playlist that has just been generated after running the program" << std::endl; // 
          list<string> laListe;
          laListe.push_back("Victor");
          laListe.push_back("Corentin");
          laListe.push_back("Thomas L");
          laListe.push_back("Thomas R");
          laListe.push_back("Clara");
          laListe.push_back("Momo");

          // Browse the list
          for(string playlist : laListe)
          {
            fichier << playlist << std::endl;
          }

          // We close the file
          fichier.close();
        }
        else  // otherwise
                cerr << "Erreur à l'ouverture !" << endl;
 
        return 0;
} 
