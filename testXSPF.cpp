// Librairies
#include <XspfWriter>

XML_Char const * const baseUri = _PT("http://example.org/");
XspfWriter * const writer = XspfWriter::makeWriter(formatter, baseUri);
writer.writeFile(_PT("PGenerator.xspf"));
XspfTrack track;
track.lendCreator(_PT("Eminem"));
track.lendCreator(_PT("Martin Garix"));
track.lendCreator(_PT("Manou"));
track.lendCreator(_PT("Soprano"));
writer.addTrack(track);
reader.parseFile(_PT("PGenerator.xspf"), NULL, baseUri);
